var fs = require('fs');

// usage: node scripts/mergegraphs.js file1.graph 1,800,1,0 file2.graph 1,-800,1,0 > output.graph

let graphs = process.argv.slice(2)
	.filter((arg, index) => index % 2 == 0)
	.map(filename => JSON.parse(fs.readFileSync(filename, 'utf8')))

let transforms = process.argv.slice(2)
	.filter((arg, index) => index % 2 == 1)
	.map(arg => {
		return {
			m: parseInt(arg.split(",")[0]),
			b: parseInt(arg.split(",")[1]),
			n: parseInt(arg.split(",")[2]),
			c: parseInt(arg.split(",")[3]),
		};
		// x will be transformed to mx+b, y to nx+c
	});

function transformItem(item, transforms, index) {
	// Ensure unique IDs
	item.id = (index + 1).toString() + item.id
	if (item.fromId) item.fromId = (index + 1).toString() + item.fromId;
	if (item.toId) item.toId = (index + 1).toString() + item.toId;
	if (item.parent) item.parent = (index + 1).toString() + item.parent;

	if (!item.pos) return item;

	let t = transforms[index]
	item.pos = {
		x: (t.m * item.pos.x + t.b),
		y: (t.n * item.pos.y + t.c),
	}
	return item;
}

let graph = {
	items: graphs.map((graph, index) => graph.items.map(item => transformItem(item, transforms, index))).flat(),
	theme: graphs[0].theme,
	jsondownload: true,
	jsonupload: true,
	contextmenu: true,
	customjson: true,
	exportcytoscape: true,
	exportsvg: true
};

console.log(JSON.stringify(graph, null, 2));