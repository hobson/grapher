var fs = require('fs');

let filename = process.argv[2]
var obj = JSON.parse(fs.readFileSync(filename, 'utf8'));

function getNode(id, obj) {
	return obj.elements.find(x => x._id === id)
}

function getEdge(id, obj) {
	return obj.connections.find(x => x._id === id)
}

let nodes = obj.maps[0].elements.map(x => {
	let node = getNode(x.element, obj);
	return {
		id: x._id,
		kind: "node",
		label: node.attributes.label,
		notes: "",
		desc: node.attributes.description + "\n\n" + node.attributes.articles?.join("\n"),
		fill: x.style.color,
		tags: [],
		parent: null,
		pos: {
			x: x.position.x,
			y: x.position.y
		}
	}
});

let edges = obj.maps[0].connections.map(x => {
	let edge = getEdge(x.connection, obj)
	// console.log({ edge });
	// console.log({ from: getNode(edge.from, obj), to: getNode(edge.to, obj) })

	return {
		id: x._id,
		kind: "edge",
		label: "",
		desc: "",
		tags: [],
		directed: (edge.direction === "directed"),
		weight: 5,
		fromId: obj.maps[0].elements.find(x => x.element === edge.from)._id,
		toId: obj.maps[0].elements.find(x => x.element === edge.to)._id
	}
});

let graph = {
	items: [...nodes, ...edges],
	theme: {
		name: "foresight",
		bgfill: "#1c170b",
		grid: false,
		selectionColor: "blue",
		highlightColor: "red",
		nodefill: "#7b9ecb",
		nodelabelstroke: "black",
		nodelabelfontsize: 16,
		nodenotesfontsize: 16,
		nodeborder: "white",
		edgestroke: "#ccc",
		edgeshape: "ortho",
		edgestroketype: "solid",
		font: "Roboto Condensed",
		badges: [
			"https://cdn.discordapp.com/attachments/987690735639875584/997881951534981203/hexbadge.png"
		]
	},
	jsondownload: true,
	jsonupload: true,
	contextmenu: true,
	customjson: false,
	exportcytoscape: true,
	exportsvg: true
}

console.log(JSON.stringify(graph, null, " "));