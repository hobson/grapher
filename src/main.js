import './app.css'
import GraphEditor from './GraphEditor.svelte';

const app = new GraphEditor({
	target: document.querySelector('#grapheditor'),
	props: {
		graphjsonpath: window.graphjsonpath,
		config: (window.overrideConfig || {mode: 'viewonly'}),
		svgForTextBBox: document.getElementById('textbbox')
	}
});

export default app;