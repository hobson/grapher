// Docs on event and context https://docs.netlify.com/functions/build/#code-your-function-2
const knownTrees = {
	nanotech: { recipient: process.env.DEFAULT_RECIPIENT, password: process.env.DEFAULT_TREE_SECRET },
	neurotech: { recipient: process.env.DEFAULT_RECIPIENT, password: process.env.DEFAULT_TREE_SECRET },
	spacetree: { recipient: process.env.DEFAULT_RECIPIENT, password: process.env.DEFAULT_TREE_SECRET },
	longevity: { recipient: process.env.DEFAULT_RECIPIENT, password: process.env.DEFAULT_TREE_SECRET },
	intcoop: { recipient: process.env.DEFAULT_RECIPIEN, password: process.env.DEFAULT_TREE_SECRET }
};

const Mailjet = require('node-mailjet');

const mailjet = new Mailjet({
	apiKey: process.env.MAILJET_APIKEY_PUBLIC,
	apiSecret: process.env.MAILJET_APIKEY_PRIVATE
});

module.exports.handler = async (event, context) => {
	// console.log({ event });

	if (event.httpMethod !== 'POST') {
		return {
			statusCode: 400,
			body: `Expecting POST request with JSON body`,
		}
	}

	const sender = event.queryStringParameters.sender;
	const treename = event.queryStringParameters.treename;
	const treesecret = event.queryStringParameters.treesecret;

	if (!treename || !knownTrees[treename]) {
		return {
			statusCode: 400,
			body: `Unrecognized treename: ${treename}`,
		}
	}

	if (knownTrees[treename].password !== treesecret) {
		return {
			statusCode: 400,
			body: `Invalid treesecret ${treesecret} for ${treename} tree`,
		}
	}

	const recipient = knownTrees[treename].recipient;
	const body = JSON.parse(event.body);
	const message = body.message;
	const graph = body.graph;
	let regex = new RegExp('[a-z0-9]+@[a-z]+\.[a-z]{2,3}');

	if (!sender || !message || !sender.trim() || !message.trim() || !regex.test(sender)) {
		return {
			statusCode: 400,
			body: `Invalid sender: ${sender} or message: ${message}`,
		}
	}

	if (!graph) {
		return {
			statusCode: 400,
			body: `Graph missing`,
		}
	}

	await mailjet
		.post('send', { version: 'v3.1' })
		.request({
			Messages: [
				{
					From: {
						Email: "no-reply@grapher.polyglot.network",
						Name: "Grapher"
					},
					To: [
						{
							Email: recipient
						}
					],
					Subject: `Changes suggested to ${treename} tree by ${sender}`,
					TextPart: `Someone claiming to be ${sender} has suggested changes for ${treename} with this comment:\n\n${message}`,
					Attachments: [
						{
							ContentType: "application/octet-stream",
							Filename: `${treename}.graph`,
							Base64Content: btoa(JSON.stringify(graph, null, 2))
						}
					]
				}
			]
		});

	return {
		statusCode: 200,
		body: `Submission handled for ${treename}: ${sender} ${message} ${recipient}`,
	}
}